#!/bin/bash

function getTime
{
    year=$(date +"%Y") #Syslog does not specify year
    case "$(echo "$1" | cut -f 1 -d " ")" in
        "Jan" ) month=01;;
        "Feb" ) month=02;;
        "Mar" ) month=03;;
        "Apr" ) month=04;;
        "May" ) month=05;;
        "Jun" ) month=06;;
        "Jul" ) month=07;;
        "Aug" ) month=08;;
        "Sep" ) month=09;;
        "Oct" ) month=10;;
        "Nov" ) month=11;;
        "Dec" ) month=12;;
    esac
    if [ ! $month ]
    then
        >&2 echo "Can't recognize mounth $(echo "$1" | cut -f 1 -d " ")"
        exit 1
    fi
    day="$(echo "$1" | cut -f 2 -d " ")"
    time="$(echo "$1" | cut -f 3 -d " ")"
    echo "$year"-"$month"-"$day"T"$time"
}

function getHost
{
    echo "$(echo "$1" | cut -f 4 -d " ")"
}

function getProcess
{
    echo "$(echo "$1" | cut -f 5 -d " " | cut -f 1 -d ":")"
}

function getPriority
{
    priostr="$(echo "$1" | cut -f 6 -d " ")"
    if [[ "$(echo $priostr | head -c 1)" = "<" && "$(echo $priostr | tail -c 2)" = ">" ]]
    then
        case "$(echo "$1" | cut -f 6 -d " " | cut -f 2 -d "<" | cut -f 1 -d ">" )" in
            "debug" ) priority=7;;
            "info" ) priority=6;;
            "notice" ) priority=5;;
            "warning" ) priority=4;;
            "warn" ) priority=4;;
            "error" ) priority=3;;
            "err" ) priority=3;;
            "crit" ) priority=2;;
            "alert" ) priority=1;;
            "emerg" ) priority=0;;
            "panic" ) priority=0;;
        esac
        if [ ! $priority ]
        then
            >&2 echo "Can't recognize priority "$(echo "$1" | cut -f 6 -d " " | cut -f 2 -d "<" | cut -f 1 -d ">" )""
            echo 8
        fi
    else
        priority=8
    fi
    echo $priority
}


function getMessage
{
    if [ $prio -eq 8 ]
    then 
        echo "$(echo "$1" | cut -f 6- -d " " | sed -r 's/\[/\\\[/g' | sed -r 's/\]/\\\]/g' | sed -r "s/'/''/g")"
    else
        echo "$(echo "$1" | cut -f 7- -d " " | sed -r 's/\[/\\\[/g' | sed -r 's/\]/\\\]/g' | sed -r "s/'/''/g")"
    fi

}

if [ ! $1 ]
then
    >&2 echo "Input file is not specified!"
    exit 1
fi
input=$1
num=1
total=$(wc -l $1 | cut -d " " -f 1)
while IFS= read -r line
do
    time="$(getTime "$line")"
    host="$(getHost "$line")"
    proc="$(getProcess "$line")"
    prio="$(getPriority "$line")"
    msg="$(getMessage "$line")"
    cqlsh cassandra0 -e "INSERT INTO linux_logs.logs (id, logTime, host, process, priority, message) VALUES (now(), '$time', '$host', '$proc', $prio, '$msg')"
    echo $num/$total
    num=$(expr $num + 1)
done < "$input"