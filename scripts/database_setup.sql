// create keyspace for application and switch into it

CREATE KEYSPACE linux_logs WITH replication = {'class': 'SimpleStrategy', 'replication_factor': 3};
USE linux_logs;

// create tables for input and output

CREATE TABLE logs (id timeuuid PRIMARY KEY, logTime timestamp, host text, process text, priority int, message text);

// add index to filtered fields

CREATE INDEX time_index ON logs (logTime);
CREATE INDEX priority_index ON logs (priority);