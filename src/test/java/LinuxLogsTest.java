import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

import static org.junit.Assert.*;

public class LinuxLogsTest
{
    private JavaSparkContext sparkCtx;
    private List<LinuxLogs.LogInfo> input;
    private List<LinuxLogs.LogCounts> expectedOutput;

    @Before
    public void init() throws IllegalArgumentException, IOException
    {
        SparkConf conf = new SparkConf();
        conf.setMaster("local[2]");
        conf.setAppName("junit");
        sparkCtx = new JavaSparkContext(conf);

        Set<Long> usedTimes = new HashSet<Long>();

        expectedOutput = new ArrayList<>();
        int size = ThreadLocalRandom.current().nextInt(128) + 128;
        for(int i = 0; i < size; ++i)
        {
            long time;
            do
            {
                time = ThreadLocalRandom.current().nextLong(1024) * 3600000;
            } while (usedTimes.contains(time));
            usedTimes.add(time);

            int priority = ThreadLocalRandom.current().nextInt(9);
            int amount = ThreadLocalRandom.current().nextInt(128) + 128;
            expectedOutput.add(new LinuxLogs.LogCounts(new Date(time), priority, amount));
        }

        input = new ArrayList<>();

        for (LinuxLogs.LogCounts out: expectedOutput)
        {
            for (int i = 0; i < out.getCount(); ++i)
            {
                long time = out.getLogTime().getTime() + ThreadLocalRandom.current().nextLong(3600000);
                input.add(new LinuxLogs.LogInfo(new Date(time), out.priority));
            }
        }
    }

    @Test
    public void test() {
        JavaRDD<LinuxLogs.LogInfo> logInfoRDD = sparkCtx.parallelize(input, 1);
        JavaRDD<LinuxLogs.LogCounts> res = LinuxLogs.countLogs(logInfoRDD);
        List<LinuxLogs.LogCounts> reslist = res.collect();
        assertEquals(expectedOutput.size(), reslist.size());
        assertTrue(reslist.containsAll(expectedOutput));
    }
}