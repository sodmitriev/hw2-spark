import com.datastax.driver.core.Session;
import com.datastax.spark.connector.japi.CassandraJavaUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import com.datastax.spark.connector.cql.CassandraConnector;

import scala.Serializable;
import scala.Tuple2;

import java.util.*;

import static com.datastax.spark.connector.japi.CassandraJavaUtil.*;

/**
 * Main class of sparkRDD application
 *
 * Calculates amount of log messages per hour and message priority
 */
public class LinuxLogs
{
    /**
     * Class to represent input data
     */
    static public class LogInfo implements Serializable
    {
        /**
         * Time of log message submission
         */
        Date logTime;

        /**
         * Log message priority
         */
        int priority;

        /**
         * Constructor
         * @param logtime Time of log message submission (has wrong case notation due to cassandra connector requirements)
         * @param priority Log message priority
         */
        public LogInfo(Date logtime, int priority)
        {
            this.logTime = logtime;
            this.priority = priority;
        }

        /**
         * Get time of log message submission
         * @return Time of log message submission
         */
        public Date getLogTime()
        {
            return logTime;
        }

        /**
         * Get log message priority
         * @return Log message priority
         */
        public int getPriority()
        {
            return priority;
        }

        /**
         * Method to compare objects of this class with other objects
         * @param o Object to compare this object with
         * @return true if objects are equal, false otherwise
         */
        @Override
        public boolean equals(Object o)
        {
            //If objects are located at the same memory location, return true straight away
            if (this == o) return true;
            //If other object is null or represents a different class, return false straight away
            if (o == null || getClass() != o.getClass()) return false;
            //Cast other object to this class
            LogInfo logInfo = (LogInfo) o;
            //Compare objects' member variables
            return logTime.equals(logInfo.logTime) &&
                    priority == logInfo.priority;
        }

        /**
         * Calculate hash code of this object
         * @return Hash code of this object
         */
        @Override
        public int hashCode()
        {
            return Objects.hash(logTime, priority);
        }
    }

    /**
     * Class to represent input data
     */
    static public class LogCounts implements Serializable
    {
        /**
         * Class to represent input data
         */
        Date logTime;

        /**
         * Log message priority
         */
        int priority;

        /**
         * Amount of messages with this time and priority
         */
        int count;

        /**
         * Constructor
         * @param logtime Time of log message submission (has wrong case notation due to cassandra connector requirements)
         * @param priority Log message priority
         * @param count Amount of messages with this time and priority
         */
        public LogCounts(Date logtime, int priority, int count)
        {
            this.logTime = logtime;
            this.priority = priority;
            this.count = count;
        }

        /**
         * Get time of log message submission
         * @return Time of log message submission
         */
        public Date getLogTime()
        {
            return logTime;
        }

        /**
         * Get log message priority
         * @return Log message priority
         */
        public int getPriority()
        {
            return priority;
        }

        /**
         * Get amount of messages with this time and priority
         * @return Amount of messages with this time and priority
         */
        public int getCount()
        {
            return count;
        }

        /**
         * Method to compare objects of this class with other objects
         * @param o Object to compare this object with
         * @return true if objects are equal, false otherwise
         */
        @Override
        public boolean equals(Object o)
        {
            //If objects are located at the same memory location, return true straight away
            if (this == o) return true;
            //If other object is null or represents a different class, return false straight away
            if (o == null || getClass() != o.getClass()) return false;
            //Cast other object to this class
            LogCounts logCounts = (LogCounts) o;
            //Compare objects' member variables
            return priority == logCounts.priority &&
                    count == logCounts.count &&
                    logTime.equals(logCounts.logTime);
        }

        /**
         * Calculate hash code of this object
         * @return Hash code of this object
         */
        @Override
        public int hashCode()
        {
            return Objects.hash(logTime, priority, count);
        }
    }

    /**
     * Method to read RDD with log messages' information from cassandra database
     * @param context Job spark context
     * @return RDD with log messages' information
     */
    static JavaRDD<LogInfo> getLogs(JavaSparkContext context)
    {
        return CassandraJavaUtil.javaFunctions(context)
                //Use keyspace "linux_logs", table "logs" and map result to LogInfo class
                .cassandraTable("linux_logs", "logs", mapRowTo(LogInfo.class))
                //Select only log message time and priority
                .select("logtime", "priority");
    }

    /**
     * Method to count amount of log messages per hour and message priority
     * @param logInfoRDD RDD with log messages' information
     * @return RDD with log messages' count
     */
    static JavaRDD<LogCounts> countLogs(JavaRDD<LogInfo> logInfoRDD)
    {
        return logInfoRDD
                //Round message time to hours
                .map((info) -> new LogInfo(new Date(info.getLogTime().getTime() - info.getLogTime().getTime() % 3600000), info.getPriority()))
                //Map log info to key-value pairs, using log info as key, and integer 1 (count) as value
                .mapToPair((info) -> new Tuple2<>(new LogInfo(info.getLogTime(), info.getPriority()), 1))
                //Reduce key-value pairs to sum all log counts' per key
                .reduceByKey(Integer::sum)
                //Map key-value pairs to output class objects
                .map((tuple) -> new LogCounts(tuple._1.getLogTime(), tuple._1.getPriority(), tuple._2));
    }

    /**
     * Save count results back to cassandra database
     * @param context Job spark context
     * @param logCountRDD RDD with calculated log message counts
     */
    static void saveLogCount(JavaSparkContext context, JavaRDD<LogCounts> logCountRDD)
    {
        //Create a connector to cassandra database for drier program
        CassandraConnector connector = CassandraConnector.apply(context.getConf());

        //Create a table for output
        try (Session session = connector.openSession()) {
            session.execute("CREATE TABLE IF NOT EXISTS linux_logs.counts (log_time timestamp, priority int, " +
                    "count int, PRIMARY KEY (log_time, priority))");
        }

        //Map logCountRdd to table rows and save to table "counts" of keyspace "linux_logs"
        CassandraJavaUtil.javaFunctions(logCountRDD)
                .writerBuilder("linux_logs", "counts", mapToRow(LogCounts.class))
                .saveToCassandra();

    }

    /**
     * Spark job entry point
     * @param args Command line arguments
     */
    public static void main(String[] args)
    {

        //Get spark master url from environment variables
        String sparkMasterUrl = System.getenv("SPARK_MASTER_URL");
        if(StringUtils.isBlank(sparkMasterUrl))
        {
            //If required environment variable is not set, throw exception ending the application
            throw new IllegalStateException("SPARK_MASTER_URL environment variable must be set.");
        }

        //Get cassandra url from environment variables
        String cassandraUrl = System.getenv("CASSANDRA_URL");
        if(StringUtils.isBlank(cassandraUrl))
        {
            //If required environment variable is not set, throw exception ending the application
            throw new IllegalStateException("CASSANDRA_URL environment variable must be set");
        }

        //Create spark configuration
        SparkConf sparkConf = new SparkConf()
                //Set application name to "linux-logs"
                .setAppName("linux-logs")
                //Set spark master url to provided url
                .setMaster(sparkMasterUrl)
                //Set cassandra url to provided url
                .set("spark.cassandra.connection.host", cassandraUrl);

        //Create a spark context based on created configuration
        JavaSparkContext sparkContext = new JavaSparkContext(sparkConf);

        //Extract RDD with input data from cassandra
        JavaRDD<LogInfo> info = getLogs(sparkContext);
        //Transform input log message information to output log message count
        JavaRDD<LogCounts> counts = countLogs(info);
        //Load calculated log message count back to cassandra database
        saveLogCount(sparkContext, counts);

    }
}
