Data science lab2 - spark
=====
## Task

### Business logic

Program which calculate linux syslog priority (is given by 7 – debug, 6 – info, 5 – notice, 4 -
warning, warn, 3 - err, error, 2 - crit, 1 - alert, 0 - emerg, panic) count by hours.

### Ingest technology

bash

### Storage technology

cassandra

### Computation technology

Spark RDD

### Report includes

1. ZIP-ed src folder with your implementation
2. Screenshot of successfully executed tests
3. Screenshots of successfully executed job and result (logs)
4. Quick build and deploy manual (commands, OS requirements etc)
5. System components communication diagram (UML or COMET)

## System components communication diagram

![System components communication diagram](./SystemComponentsCommunicationDiagram.png)

## Links

[Build and deploy manual](./BuildAndDeploy.md)

[Screenshots](./screenshots/README.md)