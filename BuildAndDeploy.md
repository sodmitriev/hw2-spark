Build and deploy manual
=======================

## Required tools

- jdk (version 7 or later)
- maven
- docker
- git

## Preparation

Download spark binaries (required only to submit job)

Preferably use [2.2.0-bin-hadoop2.7 version](https://archive.apache.org/dist/spark/spark-2.2.0/spark-2.2.0-bin-hadoop2.7.tgz)

Download project sources

```
git clone https://bitbucket.org/sodmitriev/hw2-spark
cd hw2-spark
```

Alternatively : download archived sources from [bitbucket repository](https://bitbucket.org/sodmitriev/hw2-spark)

Download required docker images and create network for cassandra database

```
docker pull cassandra:3.0.0
docker pull bde2020/spark-master:2.2.0-hadoop2.7
docker pull bde2020/spark-worker:2.2.0-hadoop2.7
docker network create cassandra
```

## Build

Build application with maven

```
mvn package
```

Alternatively : download jar file from [Bitbucket download section](https://bitbucket.org/sodmitriev/hw2-spark/downloads/)

## Start system components

### Start cassandra cluster

Setup memory usage for cassandra (change parameters depending on your system)

```
MAX_HEAP_SIZE=1G
HEAP_NEWSIZE=256M
```

Start cassandra seed node

```
docker run --name cassandra0 --network cassandra -d -e MAX_HEAP_SIZE="$MAX_HEAP_SIZE" -e HEAP_NEWSIZE="$HEAP_NEWSIZE" cassandra:3.0.0
```

Start additional nodes
 
You can change amount of nodes depending on your needs, but you might need to change cassandra settings, thence it's recommended to use 3 nodes for testing of this application

```
docker run --name cassandra1 -d --network cassandra -e CASSANDRA_SEEDS=cassandra0 -e MAX_HEAP_SIZE="$MAX_HEAP_SIZE" -e HEAP_NEWSIZE="$HEAP_NEWSIZE" cassandra:3.0.0
docker run --name cassandra2 -d --network cassandra -e CASSANDRA_SEEDS=cassandra0 -e MAX_HEAP_SIZE="$MAX_HEAP_SIZE" -e HEAP_NEWSIZE="$HEAP_NEWSIZE" cassandra:3.0.0
```

### Start spark cluster

Start spark master node

```
docker run --name spark-master --network cassandra -h spark-master -e ENABLE_INIT_DAEMON=false -d bde2020/spark-master:2.2.0-hadoop2.7
```

Start spark worker nodes

You can change amount of nodes, 3 nodes were used while testing

```
docker run --name spark-worker-1 --network cassandra --link spark-master:spark-master -e ENABLE_INIT_DAEMON=false -d bde2020/spark-worker:2.2.0-hadoop2.7
docker run --name spark-worker-2 --network cassandra --link spark-master:spark-master -e ENABLE_INIT_DAEMON=false -d bde2020/spark-worker:2.2.0-hadoop2.7
docker run --name spark-worker-3 --network cassandra --link spark-master:spark-master -e ENABLE_INIT_DAEMON=false -d bde2020/spark-worker:2.2.0-hadoop2.7
```

## Deploy

### Prepare input data

Setup cassandra keyspace for application using provided script

```
docker run --network cassandra --rm -v "$(pwd)"/scripts:/scripts -e MAX_HEAP_SIZE="$MAX_HEAP_SIZE" -e HEAP_NEWSIZE="$HEAP_NEWSIZE" cassandra:3.0.0 cqlsh cassandra0 -f /scripts/database_setup.sql
```

Generate test data using provided script

```
mkdir test_data
scripts/generate_data.sh 300 > test_data/data
```

Alternatively : use your logs in the following format

```
date hostname process: [<priority>] Message
#Examples:
#Oct 28 8:41:42 debian generator: <debug> Something happened!
#Oct 28 8:41:42 debian generator: Something happened!
```

Note that messages without priority (e.g. kernel logs) will be assigned a priority of 8

Load logs into cassandra using provided script

```
docker run --network cassandra --rm -v "$(pwd)"/scripts:/scripts -v "$(pwd)"/test_data:/test_data -e MAX_HEAP_SIZE="$MAX_HEAP_SIZE" -e HEAP_NEWSIZE="$HEAP_NEWSIZE" cassandra:3.0.0 /scripts/load_data.sh /test_data/data
```

### Submit spark job

Set cassndra seed node and spark master ip

```
export SPARK_MASTER_URL=spark://$(docker inspect -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' spark-master):7077
export CASSANDRA_URL=$(docker inspect -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' cassandra0)
```

Submit job to spark cluster (set SPARK_INSTALL_DIR to spark directory)

```
SPARK_DIR="path to spark directory"
"$SPARK_DIR"/bin/spark-submit --class LinuxLogs --master "$SPARK_MASTER_URL" target/linux_logs-jar-with-dependencies.jar
```

## Check results

```
docker run --network cassandra --rm -e MAX_HEAP_SIZE="$MAX_HEAP_SIZE" -e HEAP_NEWSIZE="$HEAP_NEWSIZE" cassandra:3.0.0 cqlsh cassandra0 -e "SELECT * FROM linux_logs.counts"
```
